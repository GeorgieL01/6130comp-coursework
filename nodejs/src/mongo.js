//Object data modelling library for mongo
const mongoose = require('mongoose');

//Mongo db client library
//const MongoClient  = require('mongodb');

//Express web service library
const express = require('express')

//used to parse the server response from json to object.
const bodyParser = require('body-parser');

//instance of express and port to use for inbound connections.
const app = express()
const port = 3000

var os = require("os");
var myhostname = os.hostname();

let listOfNodes = [];
let systemleader = 0;
let startTime = new Date().getTime();

var nodeID = Math.floor(Math.random() * (100 - 1 + 1) + 1);
let toSend = {"hostname": myhostname, "status": "alive","nodeID":nodeID};


//connection string listing the mongo servers. This is an alternative to using a load balancer. THIS SHOULD BE DISCUSSED IN YOUR ASSIGNMENT.
const connectionString = 'mongodb://localmongo1:27017,localmongo2:27017,localmongo3:27017/sweetShopDB?replicaSet=rs0';

//tell express to use the body parser. Note - This function was built into express but then moved to a seperate package.
app.use(bodyParser.json());

//connect to the cluster
mongoose.connect(connectionString, {useNewUrlParser: true, useUnifiedTopology: true});


var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));

var Schema = mongoose.Schema;

var notflixSchema = new Schema({

accountID: Number,
userName: String,
titleID: Number,
userAction: String,
dateAndTime: String,
pointOfInteraction: String,
typeOfInteraction: String

});

var notflixModel = mongoose.model('Notflix', notflixSchema, 'Notflix');

app.get('/', (req, res) => {
  notflixModel.find({},'item price quantity lastName', (err, notflix) => {
    if(err) return handleError(err);
    res.send(JSON.stringify(notflix))
  }) 
})

app.post('/',  (req, res) => { 
  var new_notflix_instance = new notflixModel(req.body); 
  new_notflix_instance.save(function (err) { 
  if (err) res.send('Error'); 
    res.send(JSON.stringify(req.body)) 
  }); 
}) 

app.put('/',  (req, res) => {
  res.send('Got a PUT request at /')
})

app.delete('/',  (req, res) => {
  res.send('Got a DELETE request at /')
})

//bind the express web service to the port specified
app.listen(port, () => {
 console.log(`Express Application listening at port ` + port)
})

//use the amqplib
var amqp = require('amqplib/callback_api');
const { json } = require('body-parser');


function leaders(){
//connect to the MQ cluster
setInterval(function(){
amqp.connect('amqp://test:test@6130comp-coursework_haproxy_1', function(error0, connection) {

    //if connection failed throw error
    if (error0) {
        throw error0;
    }

    //create a channel if connected and send hello world to the logs Q
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }
        var exchange = 'logs';
        var msg =  JSON.stringify(toSend);

        channel.assertExchange(exchange, 'fanout', {
                durable: false
        });
        
        channel.publish(exchange, '', Buffer.from(msg));
        console.log(" [x] Sent %s", msg);
     });

           
     //in 1/2 a second force close the connection
     setTimeout(function() {
         connection.close();
     }, 500);
});
},2500);

amqp.connect('amqp://test:test@6130comp-coursework_haproxy_1', function(error0, connection) {
      if (error0) {
              throw error0;
            }
      connection.createChannel(function(error1, channel) {
              if (error1) {
                        throw error1;
                      }
              var exchange = 'logs';

              channel.assertExchange(exchange, 'fanout', {
                        durable: false
                      });

              channel.assertQueue('', {
                        exclusive: true
                      }, function(error2, q) {
                                if (error2) {
                                            throw error2;
                                          }
                                console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q.queue);
                                channel.bindQueue(q.queue, exchange, '');

                                channel.consume(q.queue, function(msg) {
                                            if(msg.content) {
                                                          let t = new Date().getTime();
                                                          let x = -1 * (startTime - t) /1000;
                                                          let d = JSON.parse(msg.content.toString("utf-8")).hostname;
                                                          let n = JSON.parse(msg.content.toString("utf-8")).nodeID;
                                                          listOfNodes.some(node => node.name === d) ? (listOfNodes.find(e => e.name === d)).aliveTime = x : listOfNodes.push({"name" : d, "nodeID": n, "aliveTime":x});

                                                         console.log(listOfNodes); 
                                                        }
                                          }, {
                                                      noAck: true
                                                    });
                              });
            });
});
setInterval(function(){
  let g = new Date().getTime();
  let j = -1 * (startTime - g) /1000;
  let leader = 1;
  listOfNodes.forEach(function(potentailLeader){
  let lastRead = j - potentailLeader.aliveTime;
  if (potentailLeader.name === myhostname||lastRead > 10){
    //do nothing 
  } else {
    if (nodeID < potentailLeader.nodeID){
      leader = 0;

    }

  }

  });
  systemleader = (leader === 1) ? 1 : 0;

},5000);

setInterval(function(){
  let a = new Date().getTime();
  let b = -1 * (startTime - a)/1000;
  if (systemleader === 1) {
    console.log("I am the leader!");
    listOfNodes.forEach(function(element){
      let timeSince = b - element.aliveTime;
      if (!(element.name === myhostname)){ 
        if (timeSince >= 10){
          restartContainer(element.name);
        }
      }
    });
  }
  },6000);

function restartContainer(contName){
//restart the container using axios
console.log("Container " + contName + " is dead and needs to be restarted");
}; 

}
setTimeout(leaders, 20000);